# Моя третья задача от [Deusops](https://deusops.com/)

# Условия и ссылки
<details>
<summary>Light:</summary>

1. Разверните виртуальную машины с linux: dev01 и prod01. Можно использовать любые способы создания, например в Яндекс Облаке или локально на Vagrant
2. Установите docker на обе виртуальные машины
3. Напишите Dockerfile для проекта, придерживаясь best practices
4. Написать docker-compose.yml для запуска приложения с учетом сервисов, необходимых для запуска и сохранения данных
5. Запустить проект через docker-compose на dev01, обеспечив доступность по адресу dev.example.com
6. Установить необходимые зависимости на prod01 и запустить проект без использования compose, обеспечив доступность по адресу example.com

</details>

<details>
<summary>Normal:</summary>

1. Создайте проект на Gitlab и зарегистрируйте ваши машины в качестве раннеров для проекта, используя тэги dev и prod. Убедитесь, что gitlab-runner имеет доступ к docker
2. Напишите gitlab-ci пайплайн для автоматической сборки Docker-образа с приложением, добавьте шаг с публикацией образа в gitlab registry проекта
3. Добавьте в пайплайн шаг “deploy to dev” и организуйте запуск проекта на dev01 на прежних условиях (пункт L-5), используя образ, собранный в текущем пайплайне
4. Заведите репозиторий в docker hub и добавьте шаг “release” в пайплайн, который будет перекладывать собранный образ из gitlab registry в docker hub с добавлением тэга релизной версии образа
5. Добавьте в пайплайн шаг “deploy to prod” и организуйте запуск проекта на prod01 на прежних условиях (пункт L-6), используя образ из docker hub
6. Продумайте логику пайплайна, чтобы разные задачи соответствовали разным веткам и отвечали принципам надежной и безопасной разработки (например, деплой на prod только из мастер-ветки)

</details>

<details>
<summary>Hard:</summary>

1. Добавить шаг с деплоем “feature-окружений”: для веток, начинающихся с “feature”, собирается образ с тэгом хэша коммита, на dev-контуре поднимается копия приложения. Запущенные копии не должны конфликтовать между собой за имена, ресурсы и порты. Необходимая для подключения информация должна выводить в лог CI
2. Установить на dev-контуре docker-proxy, который будет слушать web-запросы и отправлять их в целевой контейнер
3. Настроить CI следующим образом:
    - При деплое из main или тэга обновляется domain.com на prod01
    - При деплое из develop обновляется dev.domain.com на dev01
    - При деплое из релизной и feature-веток раскатывается адрес hashcommit.dev.domain.com на dev01
4. Настроить в Gitlab CI сущность environments, которая будет автоматически создаваться для новых окружений и иметь возможность удаления окружений

</details>

<details>
<summary>Expert:</summary>

1. Сделать автогенерацию сертификатов для доменного имени сайта
2. Организовать мониторинг ресурсов и состояний ваших окружений
3. Проработать в gitlab-ci шаги с откатом окружений на предыдущие версии

</details>

<details>
<summary>Links:</summary>

- [Как поднимать виртуальные машины в Vagrant](https://gitlab.com/deusops/lessons/documentation/vagrant)
- [Создание виртуальных машин в Яндекс Клауд ](https://cloud.yandex.ru/ru/docs/tutorials/routing/web-service#create-vms)
- [Подборка информации по Docker](https://gitlab.com/deusops/lessons/documentation/docker)
- [Подборка информации по Gitlab CI](https://gitlab.com/deusops/lessons/documentation/gitlab-ci)
- [докер-прокси, позволяющий в реальном времени автоматически создавать локейшены для новых хостов](https://hub.docker.com/r/jwilder/nginx-proxy)
- [traefik [1] - может обрабатывать специальные правила, настраивать ssl и много чего еще](https://www.digitalocean.com/community/tutorials/how-to-use-traefik-as-a-reverse-proxy-for-docker-containers-on-ubuntu-18-04-ru)
- [traefik [2] - может обрабатывать специальные правила, настраивать ssl и много чего еще](https://habr.com/ru/post/551792/)
- [Вспомним о certbot и letsencrypt](https://pentacent.medium.com/nginx-and-lets-encrypt-with-docker-in-less-than-5-minutes-b4b8a60d3a71)
- [Приложение (исходники)](https://gitlab.deusops.com/deuslearn/classbook/todo-list-app)

</details>

1. **Создание/хранение инфраструктуры**:
    - По заданию 2 контура - [dev](https://gitlab.com/va1erify-deusops/task-22-03-24/infrastructure/state/infrastructure-dev) и [prod](https://gitlab.com/va1erify-deusops/task-22-03-24/infrastructure/state/infrastructure-prod).
    - Каждый контур хранится и версионируется в отдельном репозитории.
    - Развертывание происходит в Yandex Cloud с помощью Terraform.
    - Terraform backend хранится в Gitlab.
    - Gitlab [CI](https://gitlab.com/va1erify-deusops/task-22-03-24/devops/gitlabci-flows/gitlabci-flow-infrastructure) также хранится в отдельном репозитории и версионируется
    - После создания инфраструктуры, парсится terraform outputs с помощью python скрипта, и результат записывается в виде Ansible inventory. Каждый контур в отдельный репозиторий ([dev](https://gitlab.com/va1erify-deusops/task-22-03-24/variables/hosts_dev), [prod](https://gitlab.com/va1erify-deusops/task-22-03-24/variables/hosts_prod)) 

2. **Провижн инфраструктуры**:
    - [Репозиторий](https://gitlab.com/va1erify-deusops/task-22-03-24/infrastructure/provision/ansible) с Ansible хранится в отдельном репозитории и версионируется.
    - Gitlab [CI](https://gitlab.com/va1erify-deusops/task-22-03-24/devops/gitlabci-flows/gitlabci-flow-provision) для провижна также хранится в отдельном репозитории и версионируется.
    - [Ansible роль для установки и конфигурирования certboot](https://gitlab.com/va1erify-ansible-roles/ansible-role-certbot)
    - [Ansible роль для установки и конфигурирования docker](https://gitlab.com/va1erify-ansible-roles/ansible-role-docker)
    - [Ansible роль для установки и конфигурирования mysql](https://gitlab.com/va1erify-ansible-roles/ansible-role-mysql)
    - [Ansible роль для установки и конфигурирования nginx](https://gitlab.com/va1erify-ansible-roles/ansible-role-nginx)

3. **Приложение**
    - [Приложение](https://gitlab.com/va1erify-deusops/task-22-03-24/development/todolist/app) вынесено в отдельный репозиторий.
    - Gitlab [CI](https://gitlab.com/va1erify-deusops/task-22-03-24/devops/gitlabci-flows/gitlabci-flow-app) для сборки и деплоя приложения вынесен в отдельный репозиторий.
    - [Dockerfile](https://gitlab.com/va1erify-deusops/task-22-03-24/devops/docker-images/todolist) вынесен в отдельный репозиторий.
    - [Ansible для деплоя приложения](https://gitlab.com/va1erify-deusops/task-22-03-24/devops/gitlabci-flows/ansible-deploy-docker-image) вынесен в отдельный репозиторий.

## Универсальные Gitlab flow
- [Gitlab flow для сборки docker образа](https://gitlab.com/va1erify-gitlab-ci/docker-build)
- [Gitlab flow для деплоя инфры через terraform и сохранения backend в Terraform states](https://gitlab.com/va1erify-gitlab-ci/gitlab-terraform-ci)
- [Gitlab flow для запуска Ansible плейбука](https://gitlab.com/va1erify-gitlab-ci/ansible-ci)

## Используемые инструменты и технологии
- Terraform
- Ansible
- Gitlab
- Docker
- Python
- MySQL
- Certbot
- Nginx
- Traefik
- Portainer
- Yandex Cloud
- Bash

## Images
![Логотип](images/dev.va1erify.ru.png)

![Логотип](images/hashcommit.dev.va1erify.ru.png)

![Логотип](images/hashcommit2.dev.va1erify.ru.png)

![Логотип](images/dev_portainer.png)

![Логотип](images/dev_traefik.png)

![Логотип](images/prod.va1erify.ru.png)

![Логотип](images/prod.pornainer.png)

![Логотип](images/select_from_prod_table.png)

![Логотип](images/environments.png)

![Логотип](images/structure.png)